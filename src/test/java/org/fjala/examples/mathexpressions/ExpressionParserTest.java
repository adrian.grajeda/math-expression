/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fjala.examples.mathexpressions;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;

/**
 *
 * @author agrajeda
 */
public class ExpressionParserTest {

    @Test
    void parseConstExpression() {
        String input = "5.005";

        ExpressionParser parser = new ExpressionParser();

        Expression constExpression = parser.parseExpression(input);

        assertThat(constExpression.getValue(), equalTo(5.005F));
    }

    @Test
    void parseConstExpressionWithTrailingSpace() {
        String input = "   5.005";

        ExpressionParser parser = new ExpressionParser();

        Expression constExpression = parser.parseExpression(input);

        assertThat(constExpression.getValue(), equalTo(5.005F));
    }

    @Test()
    void parseConstExpressionInvalidNumber() {
        String input = "   5.005.7";

        ExpressionParser parser = new ExpressionParser();

        assertThrows(NumberFormatException.class, () -> parser.parseExpression(input));

    }

    @Test
    void parseVarExpression() {
        String input = "x";

        ExpressionParser parser = new ExpressionParser();

        Expression result = parser.parseExpression(input);

        boolean isVarExpression = result instanceof VarExpression;
        assertThat(isVarExpression, is(true));

        VarExpression varExpression = (VarExpression) result;
        assertThat(varExpression.getName(), equalTo("x"));
    }

    @Test
    void parseVarExpressionWhenVarHasAName() {

        String varName = "y_1";

        ExpressionParser parser = new ExpressionParser();

        Expression result = parser.parseExpression(varName);

        boolean isVarExpression = result instanceof VarExpression;
        assertThat(isVarExpression, is(true));

        VarExpression varExpression = (VarExpression) result;
        assertThat(varExpression.getName(), equalTo(varName));
    }

    @Test
    void parseSimpleExpression() {
        String input = "x + 5";

        ExpressionParser parser = new ExpressionParser();

        Expression simpleExpression = parser.parseExpression(input);

        assertThat(simpleExpression.getClass(), equalTo(SumExpression.class));
    }

    @Test
    void parseSimpleExpressionWithoutSpaces() {
        String input = "x-5";

        ExpressionParser parser = new ExpressionParser();

        Expression simpleExpression = parser.parseExpression(input);

        assertThat(simpleExpression.getClass(), equalTo(SubsExpression.class));
    }

    @Test
    void parseSeveralExpressions() {
        String input = "x -5/x- 3/4";

        ExpressionParser parser = new ExpressionParser();

        Expression simpleExpression = parser.parseExpression(input);

        assertThat(simpleExpression.getClass(), equalTo(DivExpression.class));

    }

    @Test
    void parseAndEvaluateSimpleExpression() {
        String input = "6 - 5";

        ExpressionParser parser = new ExpressionParser();

        Expression simpleExpression = parser.parseExpression(input);

        assertThat(simpleExpression.getClass(), equalTo(SubsExpression.class));
        assertThat(simpleExpression.getValue(), equalTo(1F));
    }

    @Test
    void parseSeveralExpressionsWithParenthesis() {
        String input = "(x - 5)/(x - 3)/4";

        ExpressionParser parser = new ExpressionParser();

        Expression simpleExpression = parser.parseExpression(input);
        // x = 2

        assertThat(simpleExpression.getValue(), equalTo(0.5f));

    }

}
