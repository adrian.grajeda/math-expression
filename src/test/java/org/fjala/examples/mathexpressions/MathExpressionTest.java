/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fjala.examples.mathexpressions;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 *
 * @author agrajeda
 */
public class MathExpressionTest {

    @Test
    void testConstExpression() {
        Expression constExpression = new ConstExpression(5);

        assertThat(constExpression.getValue(), equalTo(5f));
    }

    @Test
    void testVarExpression() {
        VarExpression varExpression = new VarExpression("x");
        Expression expression = varExpression;

        varExpression.setValue(3);
        assertThat(expression.getValue(), equalTo(3f));

        varExpression.setValue(8);
        assertThat(expression.getValue(), equalTo(8f));

    }
    
    @Test
    void testSumExpression() {
        Expression left = new ConstExpression(5f);
        VarExpression right = new VarExpression("x");
        
        Expression sum = new SumExpression(left, right);
        right.setValue(0f);
        
        assertThat(sum.getValue(), equalTo(5f));
    }

    @Test
    void testSumExpressionGetLeft() {
        Expression left = new ConstExpression(5f);
        VarExpression right = new VarExpression("x");
        
        SumExpression sum = new SumExpression(left, right);
        right.setValue(0f);
        
        assertThat(sum.getValue(), equalTo(5f));
        assertThat(sum.getLeft(), equalTo(left));
    }
    
    @Test
    void testSubsExpression() {
        VarExpression left = new VarExpression("x");
        Expression right = new ConstExpression(5f);
        
        Expression subs = new SubsExpression(left, right);
        left.setValue(10f);
        
        assertThat(subs.getValue(), equalTo(5f));
    }
    
    @Test
    void testExpressionToString() {
        Expression math = new SumExpression(new ConstExpression(5F), new VarExpression("x"));
        
        assertThat(math.toString(), equalTo("5.0 + x"));
    }
    
    
    
    

}
