/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fjala.examples.mathexpressions;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

/**
 *
 * @author agrajeda
 */
public class ExpressionEvaluatorTest {

    @ParameterizedTest
    @CsvSource({
        "x + 5,         6F,    11F",
        "x, 5F, 5F",
        "2 * x, 100000, 200000",
        "x / 2, 1F,  0.5F",
        "sin(x),    180F, 0F",
        "cos(x),    180F, -1F",
        "tan(x),    180F, 0F",
        "10/sin(x),  1F,  572,986884986F",
        "x^2,   2F,  4F",
        "2^x,   2F,  4F"
    })
    void testEvalSimpleExpressionWithVar(String expressionInput, float variableValue, float expectedResult) {
        ExpressionParser parser = new ExpressionParser();
        Expression expression = parser.parseExpression(expressionInput);

        ExpressionEvaluator evaluator = new ExpressionEvaluator(expression);
        evaluator.setVariableValue("x", variableValue);

        float result = evaluator.evaluate();
        assertThat(result, equalTo(expectedResult));

    }

}
