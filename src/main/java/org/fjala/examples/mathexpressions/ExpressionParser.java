/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fjala.examples.mathexpressions;

/**
 *
 * @author agrajeda
 */
public class ExpressionParser {

    Expression parseExpression(final String expressionText) {

        var input = expressionText.trim();

        var operator = searchOperator(input);
        if (operator != null) {
            int operatorIndex = input.indexOf(operator);
            var left = getLeftOperand(input, operatorIndex);
            var right = getRightOperand(input, operatorIndex);
            return parseCompoundExpression(left, operator, right);
        } else if (isVariable(input)) {
            return parseVariable(input);
        } else {
            return parseNumber(input);
        }

    }

    private ConstExpression parseNumber(final String input) {
        float value = Float.parseFloat(input);
        return new ConstExpression(value);
    }

    private VarExpression parseVariable(final String input) {
        return new VarExpression(input);
    }

    private boolean isVariable(String input) {
        return input.matches("^[_a-z]\\w*$");

    }

    private String getLeftOperand(String input, int operatorIndex) {
        var result = input.substring(0, operatorIndex);
        return result;
    }

    private String getRightOperand(String input, int operatorIndex) {
        var result = input.substring(++operatorIndex);
        return result;
    }

    private String searchOperator(String input) {
        if (input.contains("*")) {
            return "*";
        } else if (input.contains("/")) {
            return "/";
        } else if (input.contains("+")) {
            return "+";
        } else if (input.contains("-")) {
            return "-";
        } else {
            return null;
        }
    }

    private Expression parseCompoundExpression(
        final String leftInput,
        final String operator,
        final String rightInput) {

        final Expression left = parseExpression(leftInput);
        final Expression right = parseExpression(rightInput);

        switch (operator) {
            case "+":
                return new SumExpression(left, right);
            case "-":
                return new SubsExpression(left, right);
            case "*":
                return new MultiExpression(left, right);
            case "/":
                return new DivExpression(left, right);
            default:
                throw new IllegalArgumentException("Invalid operator");

        }

    }

}
