/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fjala.examples.mathexpressions;

/**
 *
 * @author agrajeda
 */
public class ExpressionEvaluator {
    
    private final Expression expression;
    private final VariableVisitor variableVisitor;
    
    public ExpressionEvaluator(Expression expression) {
        this.expression = expression;
        variableVisitor = new VariableVisitor();
    }
    
    public void setVariableValue(String x, float f) {
        variableVisitor.setVariableValue(x, f);
        
    }
    
    public float evaluate() {
        expression.visit(variableVisitor);
        return expression.getValue();
    }
    
}
