/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fjala.examples.mathexpressions;

/**
 *
 * @author agrajeda
 */
public class VarExpression implements Expression {

    private float value;
    private final String name;

    public VarExpression(final String name) {
        this.name = name;
    }

    @Override
    public float getValue() {
        return value;
    }

    public void setValue(float newValue) {
        value = newValue;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public void visit(Visitor visitor) {
        visitor.accept(this);
    }

}
