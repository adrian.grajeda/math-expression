/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fjala.examples.mathexpressions;

/**
 *
 * @author agrajeda
 */
public class SubsExpression extends CompoundExpression {

    public SubsExpression(Expression left, Expression right) {
        super(left, right, "-");
    }

    @Override
    public float getValue() {
        return getLeft().getValue() - getRight().getValue();
    }

}
