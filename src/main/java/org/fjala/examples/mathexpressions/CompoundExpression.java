/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fjala.examples.mathexpressions;

/**
 *
 * @author agrajeda
 */
public abstract class CompoundExpression implements Expression {

    private final Expression left;
    private final Expression right;
    private final String operator;

    public CompoundExpression(Expression left, Expression right, String operator) {
        this.left = left;
        this.right = right;
        this.operator = operator;
    }

    public Expression getLeft() {
        return left;
    }

    public Expression getRight() {
        return right;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(left.toString())
                .append(" ")
                .append(operator)
                .append(" ")
                .append(right.toString());
        return builder.toString();
    }

    @Override
    public void visit(Visitor visitor) {
        getLeft().visit(visitor);
        getRight().visit(visitor);
    }

}
