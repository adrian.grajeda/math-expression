/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fjala.examples.mathexpressions;

/**
 *
 * @author agrajeda
 */
public class ConstExpression implements Expression {

    private final float value;

    public ConstExpression(float value) {
        this.value = value;
    }

    @Override
    public float getValue() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    @Override
    public void visit(Visitor visitor) {
        
    }

}
