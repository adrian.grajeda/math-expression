/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fjala.examples.mathexpressions;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author agrajeda
 */
public class VariableVisitor implements Visitor {

    private final Map<String, Float> variablesValues = new HashMap<>();

    public void setVariableValue(String varName, float value) {
        variablesValues.put(varName, value);
    }

    @Override
    public void accept(VarExpression variable) {

        if (variablesValues.containsKey(variable.getName())) {
            float value = variablesValues.get(variable.getName());
            variable.setValue(value);
        }

    }

}
