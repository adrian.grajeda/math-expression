/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fjala.examples.mathexpressions;

/**
 *
 * @author agrajeda
 */
public class DivExpression extends CompoundExpression {

    public DivExpression(Expression left, Expression right) {
        super(left, right, "/");
    }

    @Override
    public float getValue() {
        float right = getRight().getValue();
        if (right == 0) {
            throw new IllegalArgumentException("Right operand is 0");
        }
        return getLeft().getValue() / getRight().getValue();
    }

}
